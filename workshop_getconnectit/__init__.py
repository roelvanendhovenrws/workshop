# -*- coding: utf-8 -*-
"""Documentation about Workshop GetConnectIT"""

import logging

logging.getLogger(__name__).addHandler(logging.NullHandler())

__author__ = "RWS Datalab"
__email__ = "datalab.codebase@rws.nl"
__version__ = "0.1.0"
