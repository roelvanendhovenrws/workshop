# -*- coding: utf-8 -*-
"""Documentation about the workshop_getconnectit module."""

import logging

logger = logging.getLogger(__name__)


# FIXME: put actual code here
def example() -> None:
    """My first example function.

    Returns
    -------
    None.

    """
    logger.info("Providing information about the excecution of the function.")
