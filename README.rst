#####################
Workshop GetConnectIT
#####################

.. begin-inclusion-intro-marker-do-not-remove

Workshop Uitzetting Bruggen voor GetConnectIT

.. end-inclusion-intro-marker-do-not-remove


.. begin-inclusion-usage-marker-do-not-remove

How to use
----------

Provide user documentation here.

.. end-inclusion-usage-marker-do-not-remove


.. begin-inclusion-installation-marker-do-not-remove

Installation
------------

To install workshop-getconnectit, do:

.. code-block:: console

  git clone https://gitlab.com/rwsdatalab/workshop-getconnectit.git
  cd workshop-getconnectit
  pip install .

Run tests (including coverage) with:

.. code-block:: console

  pip install -r requirements-dev.txt
  pytest

.. end-inclusion-installation-marker-do-not-remove


Documentation
-------------

Include a link to your project's full documentation here.


.. begin-inclusion-license-marker-do-not-remove

License
-------

Copyright (c) 2023, Rijkswaterstaat



.. end-inclusion-license-marker-do-not-remove
