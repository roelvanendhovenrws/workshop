.. Workshop GetConnectIT documentation master file

Workshop GetConnectIT's documentation
=====================================

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex` or :ref:`modindex`.
- Report bugs with Workshop GetConnectIT in our `issue tracker <https://gitlab.com/rwsdatalab/workshop-getconnectit/-/issues>`_.
- See this document as `pdf <workshop-getconnectit.pdf>`_.

.. toctree::
   :maxdepth: 1
   :caption: First steps

   Installation <installation.rst>
   Usage <usage.rst>

.. toctree::
   :maxdepth: 1
   :caption: Algemeen

   Use case <use_case.rst>
   Privacy <privacy.rst>
   AI Impact Assessment <aiia.rst>

.. toctree::
   :maxdepth: 1
   :caption: Model

   Input <input.rst>
   Model <model.rst>
   Output <output.rst>

.. toctree::
   :maxdepth: 1
   :caption: Resultaten

   Resultaten <results.rst>

.. toctree::
   :maxdepth: 1
   :caption: All the rest

   API <apidocs/workshop_getconnectit.rst>
   Contributing <contributing.rst>
   License <license.rst>
   Release notes <changelog.rst>
