Model
=====

[BESCHRIJF HET GEKOZEN MODEL]

Randvoorwaarden:

	* Keuze model en onderliggende (hyper)parameters is onderbouwd en sluit aan bij het beoogde doel.

	* Model wordt alleen gebruikt waarvoor het bedoeld is en geëvalueerd met prestatiecriteria.

	* Keuze voor modeltype is getoetst door middel van peer review.

	* De aannames bij het modeltype zijn in overeenstemming met de eigenschappen van de data/beoogde doel.


Trainen
'''''''

[BESCHRIJF HET TRAININGSPROCES]


Evalueren training
''''''''''''''''''

[BESCHRIJF EVALUATIE VAN DE TRAINING]


Post-processing
'''''''''''''''

[BESCHRIJF POST-PROCESSING STAPPEN]


Robuustheid
'''''''''''

[BESCHRIJF ROBUUSTHEID VAN HET MODEL]
